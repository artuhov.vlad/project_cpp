#include <iostream>
#include <random>
#include <time.h>
#include "Animals.h"

using namespace std;
#define N 5


int main()
{
	//homework 19.5

	Animal** array = new Animal*[N];
	srand(time(0));

	for (int i = 0; i < N; i++)
	{
		int temp = rand() % 3;
		switch (temp)
		{
		case 0:
			array[i] = new Dog();
			break;
		case 1:
			array[i] = new Cat();
			break;
		case 2:
			array[i] = new Cow();
			break;
		default:
			break;
		}	
		array[i]->Voice();
	}

	delete[] array;
	return 0;
}
