#pragma once
#include <iostream>
#include <math.h>

class Vector
{
public:
	Vector() :x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void print();
	double length();

private:
	double x;
	double y;
	double z;

};

