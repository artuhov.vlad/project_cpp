#include "Animals.h"

void Animal::Voice()
{
	std::cout << "Base text" << std::endl;
}

void Dog::Voice()
{
	std::cout << "Woof!" << std::endl;
}

void Cat::Voice()
{
	std::cout << "Meeow!" << std::endl;
}

void Cow::Voice() 
{
	std::cout << "Mooo!" << std::endl;
}
