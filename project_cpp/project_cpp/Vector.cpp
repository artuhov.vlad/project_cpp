#include "Vector.h"

void Vector::print()
{
	std::cout << "X: " << x << std::endl;
	std::cout << "Y: " << y << std::endl;
	std::cout << "Z: " << z << std::endl;
}

double Vector::length()
{
	return sqrt(x * x + y * y + z * z);
}
