#include "Player.h"

std::string Player::getName()
{
    return name;
}

void Player::setName(std::string val)
{
    name = val;
}

int Player::getScore()
{
    return score;
}

void Player::setScore(int val)
{
    score = val;
}
