#pragma once
#include <iostream>



class Animal
{
public:
	Animal() {};

	virtual void Voice();
};

class Dog : public Animal
{
public:
	Dog() 
	{};

	void Voice() override;
};

class Cat : public Animal
{
public:
	Cat() {};

	void Voice() override;
};

class Cow : public Animal
{
public:
	Cow() {};

	void Voice() override;
};