#pragma once
#include <string>
class Player
{
public:
	Player() : score(0), name("")
	{}

	std::string getName();
	void setName(std::string val);
	int getScore();
	void setScore(int val);

private:
	int score;
	std::string name;
};

